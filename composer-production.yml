frversion: '3'
services:
  proxy:
    image: nginx:1.15.3
    container_name: phenex_proxy
    hostname: proxy
    networks:
      frontend:
        ipv4_address: 172.21.0.5
    depends_on:
      - api
      - frontend
      - backend
      - resfinder_frontend
    ports:
      - "801:80"
    volumes:
      - ./proxy/nginx.conf:/etc/nginx/nginx.conf
      - ./proxy/proxy.nginx.conf:/etc/nginx/conf.d/default.conf

  rabbitmq:
    image: rabbitmq:3.8.2-management
    container_name: phenex_rabbitmq
    hostname: rabbitmq
    networks:
      backend:
        ipv4_address: 172.22.0.5
    ports:
      - 5672:5672
      - 15672:15672

  redis:
    networks:
      backend:
        ipv4_address: 172.22.0.6
    image: redis:5.0.4
    restart: always
    container_name: phenex_test_redis
    hostname: redis-test
    environment:
      TZ: Europe/Copenhagen
    volumes:
      - ./db/redis:/data

  mongo:
    networks:
      backend:
        ipv4_address: 172.22.0.7
    image: mongo:3.7.7
    restart: always
    container_name: phenex_mongo
    env_file: db/.env.secrets
    environment:
      TZ: Europe/Copenhagen
    volumes:
      - ./db/mongo:/data/db

  chromedriver:
    networks:
      backend:
        ipv4_address: 172.22.0.8
    image: robcherry/docker-chromedriver
    restart: always
    privileged: true
    container_name: phenex_backend_chromedriver
    environment:
      - CHROMEDRIVER_WHITELISTED_IPS=''

  resfinder_frontend:
    networks:
      frontend:
        ipv4_address: 172.21.0.6
    ports:
      - "8090:8090"
    image: genomicepidemiology/phenex-resfinder-frontend:prod
    restart: always
    container_name: phenex_resfinder_frontend
    hostname: resfinder-frontend
    build:
      context: resfinder-frontend/
      dockerfile: docker/Dockerfile.development
    command: sh -c "/app/scripts/startup.sh"
    volumes:
      - ./resfinder-frontend/:/app
    environment:
      - VUE_APP_AUTH_URL=//phenex.docker

  frontend:
    networks:
      backend:
      frontend:
        ipv4_address: 172.21.0.7
    image: genomicepidemiology/phenex-frontend:prod
    restart: always
    container_name: phenex_frontend
    build:
      context: frontend/
      dockerfile: docker/Dockerfile.production
      args:
        DEPLOYMENT: production
    environment:
      HOST: phenex.docker
      DEBUG: 'true'
      DEPLOYMENT: production
      TZ: Europe/Copenhagen
      LANG: C.UTF-8
      LC_ALL: C.UTF-8

  backend:
    networks:
      backend:
      frontend:
        ipv4_address: 172.21.0.8
    image: genomicepidemiology/phenex-backend:prod
    restart: always
    container_name: phenex_backend
    build:
      context: backend/
      dockerfile: docker/Dockerfile.production
      args:
        DEPLOYMENT: production
    volumes:
      - ./backend/:/src
    depends_on:
      - mongo
    env_file: backend/.env.secrets
    environment:
      HOST: phenex.docker
      DEBUG: 'false'
      DEPLOYMENT: production
      TZ: Europe/Copenhagen
      LANG: C.UTF-8
      LC_ALL: C.UTF-8

  api:
    networks:
      backend:
      frontend:
        ipv4_address: 172.21.0.9
    image: genomicepidemiology/phenex-api:prod
    restart: always
    container_name: phenex_api
    build:
      context: api/
      dockerfile: docker/Dockerfile.production
    volumes:
      - ./api/:/src
    depends_on:
      - mongo
    env_file: api/.env.secrets
    environment:
      HOST: phenex.docker
      DEBUG: 'true'
      DEPLOYMENT: production
      TZ: Europe/Copenhagen
      LANG: C.UTF-8
      LC_ALL: C.UTF-8
      MONGO_HOST: mongo
      MONGO_PORT: 27017
      MONGO_DB_NAME: phenex
      REDIS_HOST: redis
      REDIS_PORT: 6379
      MAIL_PORT: 587
      MAIL_SERVER: smtp.mailgun.org
      MAIL_SENDER: noreply@food.dtu.dk
      TESTING_EMAIL: l_dynowski@yahoo.com
      TESTING_PASSWORD: 123456
      MIN_CODE_COVERAGE: 85

networks:
  frontend:
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: 172.21.0.0/24
  backend:
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: 172.22.0.0/24
